import React from 'react';

import { Container, ProductTable, Total } from './styles';
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md'


export default function Cart() {
  return (
    <Container>
      <ProductTable>
        <thead>
          <tr>
            <th />
            <th>Produto</th>
            <th>QTD</th>
            <th>subtotal</th>
            <th />
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <img src="https://static.netshoes.com.br/produtos/tenis-nike-revolution-5-masculino/26/HZM-1731-026/HZM-1731-026_zoom2.jpg?ts=1571078789&ims=326x" alt="Tenis" />
            </td>
            <td>
              <strong>Tenis muito massa</strong>
              <span>R$129,90</span>
            </td>
            <td>
              <div>
                <button type="button" >
                  <MdRemoveCircleOutline size={20} colo="#7159c1" />
                </button>
                <input type="number" readOnly value={2} />
                <button type="button" >
                  <MdAddCircleOutline size={20} colo="#7159c1" />
                </button>
              </div>
            </td>
            <td>
              <stron>R$258,80</stron>
            </td>
            <td>
              <button type="button">
                <MdDelete size={20} color="#7159c1" />
              </button>
            </td>
          </tr>
        </tbody>

      </ProductTable>

      <footer>
        <button type="button">Finalizar Pedido</button>
        <Total>
          <span>Total</span>
          <strong>R$1920,20</strong>
        </Total>
      </footer>
    </Container>
  );
}
