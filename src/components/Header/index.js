//Imports
import React from 'react';
import { Link } from 'react-router-dom'
//icon
import {MdShoppingBasket} from 'react-icons/md'
//Styles
import { Container,Cart } from './styles';
//images
import logo from '../../assets/images/logo.svg'


export default function Header() {
    return (
        <Container>
            <Link to='/'>
                <img src={logo} alt='RocktShoes' />
            </Link>
            <Cart>
                <div>
                    <strong>Meu Carrinho </strong>
                    <span>3 Itens</span>
                </div>
                <MdShoppingBasket size={36} color="#FFF"/>
            </Cart>
        </Container>
    );
}
